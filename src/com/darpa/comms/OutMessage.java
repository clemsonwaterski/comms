package com.darpa.comms;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.google.android.gms.maps.model.LatLng;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;


public class OutMessage implements Parcelable {

	private int messageID;
	private int selfID;
	private float selfTime;
	private Location selfLocation;
	private List<LatLng> obstacleList = new ArrayList<LatLng>();
	private int leaderID;
	private double leaderDistance;		//used during leader election
	private List<Integer> childrenIDs = new ArrayList<Integer>();
	private List<LatLng> sinkList = new ArrayList<LatLng>();
	private Map<Integer, Integer> robotParent = new LinkedHashMap<Integer, Integer>();
	private boolean callingElection;
	private Map<Integer, LatLng> UVPositionMappings;
	private Integer generation;
	

	
	//This constructor has everything
	public OutMessage(int _selfID, Location _selfLocation,
			ArrayList<LatLng> _obstacleList, int _leaderID, Double _leaderDistance, 
			ArrayList<Integer> _childrenIDs, List<LatLng> _sinkList, 
			Map<Integer,Integer> _robotParent, boolean _callingElection,
			Map<Integer, LatLng> _UVPositionMappings, Integer _generation) {
		this.messageID++;
		this.selfID = _selfID;
		this.selfTime = System.currentTimeMillis();
		this.selfLocation = _selfLocation;
		this.obstacleList = _obstacleList;
		this.leaderID = _leaderID;
		this.leaderDistance = _leaderDistance;
		this.robotParent = _robotParent;
		this.childrenIDs = _childrenIDs;
		this.sinkList = _sinkList;
		this.callingElection = _callingElection;
		this.UVPositionMappings=_UVPositionMappings;
		this.generation = _generation;
	}
	
	//used for passing Map information
	public OutMessage(int _selfID, Location _selfLocation,
			ArrayList<LatLng> _obstacleList, int _leaderID) {
		this.messageID++;
		this.selfID = _selfID;
		this.selfTime = System.currentTimeMillis();
		this.selfLocation = _selfLocation;
		this.obstacleList = _obstacleList;
		this.leaderID = _leaderID;
	}
	
	//Used when we do not want to pass the map
	public OutMessage(int _selfID, Location _selfLocation,
			Integer _leaderID, Double _leaderDistance, List<Integer> _childernIDs, List<LatLng> _sinkList,
			 Map<Integer, Integer> _robotParent, boolean _callingElection,
			 Map<Integer, LatLng> _UVPositionMappings, Integer _generation) {
		this.messageID++;
		this.selfID = _selfID;
		this.selfTime = System.currentTimeMillis();
		this.selfLocation = _selfLocation;
		this.obstacleList = null;
		this.leaderID = _leaderID;
		this.leaderDistance = _leaderDistance;
		this.childrenIDs = _childernIDs;
		this.sinkList = _sinkList;
		this.robotParent = _robotParent;
		this.callingElection = _callingElection;
		this.UVPositionMappings=_UVPositionMappings;
		this.generation=_generation;
	}
	
	
	@Override
	public int describeContents() {
		return 0;
	}
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(messageID);
		dest.writeInt(selfID);
		dest.writeFloat(selfTime);
		dest.writeParcelable(selfLocation,PARCELABLE_WRITE_RETURN_VALUE);
		dest.writeList(obstacleList);
		dest.writeInt(leaderID);
		dest.writeDouble(leaderDistance);
		dest.writeList(childrenIDs);
		dest.writeList(sinkList);
		dest.writeMap(robotParent);
		if(callingElection)
			dest.writeInt(1);			//there is no writeBool, so substitute with 1 or 0
		else
			dest.writeInt(0);
		dest.writeMap(UVPositionMappings);
		dest.writeInt(generation);
	}
	
	public static Parcelable.Creator<OutMessage> CREATOR = new Parcelable.Creator<OutMessage>() {

		@Override
		public OutMessage createFromParcel(Parcel source) {
			int messageID = source.readInt();
			int selfID = source.readInt();
			float selfTime = source.readFloat();
			Location selfLocation = source.readParcelable(Location.class.getClassLoader());
			ArrayList<LatLng> obstacleList = new ArrayList<LatLng>();
			obstacleList.addAll(source.readArrayList(LatLng.class.getClassLoader()));
			int leaderID = source.readInt();
			double leaderDistance = source.readDouble();
			ArrayList<Integer> childrenIDs = new ArrayList<Integer>();
			childrenIDs.addAll(source.readArrayList(Integer.class.getClassLoader()));
			ArrayList<LatLng> sinkList = new ArrayList<LatLng>();
			sinkList.addAll(source.readArrayList(LatLng.class.getClassLoader()));
			Map<Integer, Integer> robotParent = new LinkedHashMap<Integer, Integer>();
			robotParent.putAll(source.readHashMap(Integer.class.getClassLoader()));
			int intElectionCall = source.readInt();
			boolean callingElection;
			if(intElectionCall==1)
				callingElection = true;
			else
				callingElection = false;
			Map<Integer, LatLng> UVPositionMappings = new LinkedHashMap<Integer, LatLng>();
			UVPositionMappings.putAll(source.readHashMap(LatLng.class.getClassLoader()));
			Integer generation = source.readInt();
			
			return new OutMessage(selfID, selfLocation, obstacleList, leaderID, leaderDistance, childrenIDs, sinkList, 
					robotParent,callingElection,UVPositionMappings, generation);
		}

		@Override
		public OutMessage[] newArray(int size) {
			return new OutMessage[size];
		}
		
	};
	
	
}

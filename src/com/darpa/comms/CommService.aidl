package com.darpa.comms;

import com.darpa.comms.CommServiceReporter;
import com.darpa.comms.OutMessage;

interface CommService {
	void add(CommServiceReporter reporter);
	void remove(CommServiceReporter reporter);
	void sendMessage(in OutMessage outMessage);
}
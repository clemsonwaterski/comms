package com.darpa.comms;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.google.android.gms.maps.model.LatLng;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;

public class InMessage implements Parcelable{
			
	private int receivedMessageID;
	private int receivedID;				//other robots id
	private float selfTime;				//my time
	private float receivedTime;			//other robots time aka message id
	private Location receivedLocation;   //other robots location
	private List<LatLng> receivedObstacleList = new ArrayList<LatLng>();
	private int receivedLeaderID;		//other robots leader
	private double receivedLeaderDistance;		//used for leader election
	private Map<Integer, Integer> receivedParents = new LinkedHashMap<Integer, Integer>();		//other robots parent
	private List<Integer> receivedChildrenIDs;  //children of other robot
	private List<LatLng> receivedSinkList = new ArrayList<LatLng>();
	private boolean electionCalled;
	private Map<Integer, LatLng> UVPositionMappings;
	private Integer receivedGeneration;
	
	
	//This constructor used on first receive of data
	public InMessage(int receivedMessageID, int receivedID, float receivedTime,
			Location receivedLocation, List<LatLng> receivedObstacleList,
			int receivedLeaderID, double _receivedLeaderDistance, 
			Map<Integer, Integer> _receivedParents, List<Integer> receivedChildrenIDs, 
			List<LatLng> receivedSinkList, boolean _electionCalled,
			Map<Integer, LatLng> _UVPositionMappings, Integer _receivedGeneration) {
		super();
		this.receivedMessageID = receivedMessageID;
		this.receivedID = receivedID;
		this.selfTime = System.currentTimeMillis();
		this.receivedTime = receivedTime;
		this.receivedLocation = receivedLocation;
		this.receivedObstacleList = receivedObstacleList;
		this.receivedLeaderID = receivedLeaderID;
		this.receivedLeaderDistance = _receivedLeaderDistance;
		this.receivedParents = _receivedParents;
		this.receivedChildrenIDs = receivedChildrenIDs;
		this.receivedSinkList = receivedSinkList;
		this.electionCalled=_electionCalled;
		this.UVPositionMappings=_UVPositionMappings;
		this.receivedGeneration = _receivedGeneration;
	}
	
	//This constructor includes selfTime - is intended for passing message already received between packages
	public InMessage(int receivedMessageID, int receivedID, float selfTime, float receivedTime,
			Location receivedLocation, List<LatLng> receivedObstacleList,
			int receivedLeaderID, double _receivedLeaderDistance, 
			Map<Integer, Integer> _receivedParents, List<Integer> receivedChildrenIDs, 
			List<LatLng> receivedSinkList, boolean _electionCalled,
			Map<Integer, LatLng> _UVPositionMappings, Integer _receivedGeneration) {
		super();
		this.receivedMessageID = receivedMessageID;
		this.receivedID = receivedID;
		this.selfTime = selfTime;
		this.receivedTime = receivedTime;
		this.receivedLocation = receivedLocation;
		this.receivedObstacleList = receivedObstacleList;
		this.receivedLeaderID = receivedLeaderID;
		this.receivedLeaderDistance = _receivedLeaderDistance;
		this.receivedParents = _receivedParents;
		this.receivedChildrenIDs = receivedChildrenIDs;
		this.receivedSinkList = receivedSinkList;
		this.electionCalled = _electionCalled;
		this.UVPositionMappings = _UVPositionMappings;
		this.receivedGeneration = _receivedGeneration;
	}
	
	//Do something with messageID
	
	public int getReceivedMessageID() {
		return receivedMessageID;
	}
	public int getReceivedID() {
		return receivedID;
	}
	public float getReceivedTime() {
		return receivedTime;
	}
	public Location getReceivedLocation() {
		return receivedLocation;
	}
	public boolean isElectionCalled() {
		return electionCalled;
	}
	public LatLng getUVPositionMapping(Integer robot) {
		return UVPositionMappings.get(robot);
	}
	public List<LatLng> getReceivedObstacleList() {
		return receivedObstacleList;
	}
	public int getReceivedLeaderID() {
		return receivedLeaderID;
	}
	public List<Integer> getReceivedChildrenIDs() {
		return receivedChildrenIDs;
	}
	public List<LatLng> getReceivedSinkList() {
		return receivedSinkList;
	}
	public Integer getReceivedGeneration(){
		return receivedGeneration;
	}
	public Double getReceivedLeaderDistance(){
		return receivedLeaderDistance;
	}
	public Map<Integer, Integer> getReceivedParents(){
		return receivedParents;
	}


	@Override
	public int describeContents() {
		return 0;
	}
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(receivedMessageID);
		dest.writeInt(receivedID);
		//dest.writeFloat(selfTime);  //messages need to be marked with the time they arrive
		dest.writeFloat(receivedTime);
		dest.writeParcelable(receivedLocation,PARCELABLE_WRITE_RETURN_VALUE);  //need this flag?
		dest.writeList(receivedObstacleList);
		dest.writeInt(receivedLeaderID);
		dest.writeDouble(receivedLeaderDistance);
		dest.writeList(receivedChildrenIDs);
		dest.writeList(receivedSinkList);
		dest.writeMap(receivedParents);
		if(electionCalled)
			dest.writeInt(1);			//there is no writeBool, so substitute with 1 or 0
		else
			dest.writeInt(0);
		dest.writeMap(UVPositionMappings);
		dest.writeInt(receivedGeneration);
	}
	
	
	public static Parcelable.Creator<InMessage> CREATOR = new Parcelable.Creator<InMessage>() {

		@Override
		public InMessage createFromParcel(Parcel source) {
			int receivedMessageID = source.readInt();
			int receivedID = source.readInt();
			//float selfTime = source.readFloat();
			float receivedTime = source.readFloat();
			Location receivedLocation = source.readParcelable(Location.class.getClassLoader());
			ArrayList<LatLng> receivedObstacleList = new ArrayList<LatLng>();
			receivedObstacleList.addAll(source.readArrayList(LatLng.class.getClassLoader()));
			int receivedLeaderID = source.readInt();
			double receivedLeaderDistance = source.readDouble();
			ArrayList<Integer> receivedChildrenIDs = new ArrayList<Integer>();
			receivedChildrenIDs.addAll(source.readArrayList(Integer.class.getClassLoader()));
			ArrayList<LatLng> receivedSinkList = new ArrayList<LatLng>();
			receivedSinkList.addAll(source.readArrayList(LatLng.class.getClassLoader()));
			Map<Integer, Integer> receivedParents = new LinkedHashMap<Integer, Integer>();
			receivedParents.putAll(source.readHashMap(Integer.class.getClassLoader()));
			int intElectionCall = source.readInt();
			boolean electionCalled;
			if(intElectionCall==1)
				electionCalled = true;
			else
				electionCalled = false;
			Map<Integer, LatLng> UVPositionMappings = new LinkedHashMap<Integer, LatLng>();
			UVPositionMappings.putAll(source.readHashMap(LatLng.class.getClassLoader()));
			int receivedGeneration = source.readInt();
			
			
			return new InMessage(receivedMessageID, receivedID, receivedTime,
					receivedTime, receivedLocation, receivedObstacleList,
					receivedLeaderID, receivedLeaderDistance, receivedParents, receivedChildrenIDs, 
					receivedSinkList, electionCalled, UVPositionMappings, receivedGeneration);
		}

		@Override
		public InMessage[] newArray(int size) {
			return new InMessage[size];
		}
		
	};
	
}

package com.darpa.comms;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.os.SystemClock;
import android.util.Log;

import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialPort;
import com.hoho.android.usbserial.driver.UsbSerialProber;
import com.hoho.android.usbserial.util.SerialInputOutputManager;

public class MainService extends Service {

	private String tag = "Comms";
	private InMessage inMessage;
	//private String message;
	
	
	@Override
	public IBinder onBind(Intent intent) {
		Log.d(tag, "Comms bind");
    	serviceThread = new ServiceThread();
		serviceThread.start();
		return binder;
	}
	
private List<CommServiceReporter> reporters = new ArrayList<CommServiceReporter>();
	
	private class ServiceThread extends Thread {
		
		@Override public void run() {
			while(!isInterrupted()) {
				List<CommServiceReporter> targets;
				synchronized (reporters) {
					targets = new ArrayList<CommServiceReporter>(reporters);
				}
				for(CommServiceReporter commServiceReporter : targets) {
					///////////////////When a message is received, this pushes it up
					/////to the calling service
					//if(message!=null){
						//synchronized(message){
					if(inMessage!=null){
						synchronized(inMessage){
							try {
								commServiceReporter.reportInMessage(inMessage);
								//commServiceReporter.reportInMessage(message);
								inMessage=null;
							} catch (RemoteException e) {
								e.printStackTrace();
							}
						}
						
					}
					//////////////////
				}
			}
			Log.d(tag, "ServiceThread in Xbee interrupted");
		}
	}
	
	private ServiceThread serviceThread;
	
	private CommService.Stub binder = new CommService.Stub() {
		@Override public void add(CommServiceReporter reporter) throws RemoteException {
			synchronized (reporters) {
				reporters.add(reporter);
			}
		}

		@Override public void remove(CommServiceReporter reporter) throws RemoteException {
			synchronized (reporters) {
				reporters.remove(reporter);
			}
		}

		@Override
		public void sendMessage(OutMessage outMessage) throws RemoteException {
			Log.d(tag,"In Stub, Sending OutMessage");
			sendData(outMessage);
		}
	};
	

	@Override
	public void onDestroy() {
		if (serviceThread != null) {
			serviceThread.interrupt();
			serviceThread = null;
		}
		Log.d(tag, "Xbee onDestroy");
		super.onDestroy();
	}
	
	

	
	
	//////Main Communications functions//////////////////////////////////////////////
	public void sendData(OutMessage outMessage) {
        Log.d(tag,"Send Data main function");
        byte[] data = ParcelableUtil.marshall(outMessage);
        sendBytes(data);
    }
	
	private void updateReceivedData(byte[] data) {
		Log.d(tag,"Reading Something!");
        
		Parcel parcel = ParcelableUtil.unmarshall(data);
		inMessage = InMessage.CREATOR.createFromParcel(parcel);
		
		//Used for testing by returning string value
		/*final String text = new String(data);
	        if(message!=null){
	        	synchronized(message){
	        		message.concat(text);
	        	}
	        }else
	        	message=text;
        Log.i("DATA",message);*/
    }
	
	
	
	private static UsbSerialPort sPort = null;
    private List<UsbSerialPort> mEntries = new ArrayList<UsbSerialPort>();


    private static final int BUFSIZ = 4096;
    private final ByteBuffer mWriteBuffer = ByteBuffer.allocate(BUFSIZ);
    private UsbSerialPort mDriver;

    private final ExecutorService mExecutor = Executors.newSingleThreadExecutor();

    private SerialInputOutputManager mSerialIoManager;

    private final SerialInputOutputManager.Listener mListener =
            new SerialInputOutputManager.Listener() {

        @Override
        public void onRunError(Exception e) {
            Log.d(tag, "Runner stopped.");
        }

        @Override
        public void onNewData(final byte[] data) {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    updateReceivedData(data);
                }
            });
            thread.start();
        }
    };

    
    

    @Override
	public void onCreate() {
		super.onCreate();
        
        mUsbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
        mEntries = refreshDeviceList();
        
        int i=0;
        boolean connectedFlag=false;
        Log.d(tag,"Entered Loop");
        while(!connectedFlag){
        	//Loop through options to find correct device
       try{
    	  // Log.i(tag,mEntries.get(i).toString());
    	   if(!mEntries.isEmpty()){
        	final UsbSerialPort port = mEntries.get(i);
         
        	if(mEntries.get(i).toString().contains("Ftdi")){
        		//connectedFlag=true;
        		sPort=port;
        	    }	
    	   }
        	
       }catch(Exception e){
    	   Log.e(tag,"Exception");
    	   e.printStackTrace();
       }
       if (i >= mEntries.size()) {
           Log.w(tag, "Illegal position.");
           return;
       }
       //}  
        Log.d(tag,"Exit Loop");
        
        
        if (sPort == null) {
            Log.i(tag, "No serial device.");
        } else {
            final UsbManager usbManager = (UsbManager) getSystemService(Context.USB_SERVICE);

            UsbDeviceConnection connection = usbManager.openDevice(sPort.getDriver().getDevice());
            if (connection == null) {
            	Log.i(tag,"Opening device failed");
                return;
            }

            try {
                sPort.open(connection);
                sPort.setParameters(9600, 8, UsbSerialPort.STOPBITS_1, UsbSerialPort.PARITY_NONE);
                connectedFlag=true;
                Log.i(tag,"Device Conected!!");
            } catch (IOException e) {
                Log.e(tag, "Error setting up device: " + e.getMessage(), e);
                try {
                    sPort.close();
                } catch (IOException e2) {
                    // Ignore.
                }
                sPort = null;
                return;
            }
        }}
            Log.i(tag,"Serial device: " + sPort.getClass().getSimpleName());
        
        onDeviceStateChange();
 
        
    }

    
    public void sendBytes(byte[] messageByte){
        Log.i(tag,"GOT TO SEND");
        try {
        	if(mSerialIoManager!=null){
        		mSerialIoManager.writeAsync(messageByte);
        	}
        	else{
        		Log.i(tag,"mSerialIOManager is null!");
        	}
        		
        } catch (Exception e){
            mListener.onRunError(e);
        }
    }


    public void writeAsync(byte[] data) throws IOException {
        byte[] outBuff = null;
        synchronized (mWriteBuffer) {
            int len = mWriteBuffer.position();
            mWriteBuffer.put(data);
            outBuff = new byte[len];
            mWriteBuffer.rewind();
            mWriteBuffer.get(outBuff, 0, len);
            mWriteBuffer.clear();
            mDriver.write(outBuff, 200);
        }
    }

    

    private void stopIoManager() {
        if (mSerialIoManager != null) {
            Log.i(tag, "Stopping io manager ..");
            mSerialIoManager.stop();
            mSerialIoManager = null;
        }
    }

    private void startIoManager() {
        if (sPort != null) {
            Log.i(tag, "Starting io manager ..");
            mSerialIoManager = new SerialInputOutputManager(sPort, mListener);
            mExecutor.submit(mSerialIoManager);
        }
    }

    private void onDeviceStateChange() {
        stopIoManager();
        startIoManager();
    }


    private UsbManager mUsbManager;


    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private List<UsbSerialPort> refreshDeviceList() {
        

       // new AsyncTask<Void, Void, List<UsbSerialPort>>() {
       //     @Override
       //     protected List<UsbSerialPort> doInBackground(Void... params) {
                Log.d(tag, "Refreshing device list ...");
                SystemClock.sleep(1000);

                final List<UsbSerialDriver> drivers =
                        UsbSerialProber.getDefaultProber().findAllDrivers(mUsbManager);

                final List<UsbSerialPort> result = new ArrayList<UsbSerialPort>();
                for (final UsbSerialDriver driver : drivers) {
                    final List<UsbSerialPort> ports = driver.getPorts();
                    Log.d(tag, String.format("+ %s: %s port%s",
                            driver, Integer.valueOf(ports.size()), ports.size() == 1 ? "" : "s"));
                    result.addAll(ports);
                    
                }

                return result;
            }
	
	
	
	
	


    //http://stackoverflow.com/questions/18000093/how-to-marshall-and-unmarshall-a-parcelable-to-a-byte-array-with-help-of-parcel
    
    public static class ParcelableUtil{
    	
    	public static byte[] marshall(Parcelable parcelable){
    		Parcel parcel = Parcel.obtain();
    		parcelable.writeToParcel(parcel, 0);
    		byte[] bytes = parcel.marshall();
    		parcel.recycle();  //not sure if needed
    		return bytes;
    	}
    	
    	public static Parcel unmarshall(byte[] bytes){
			Parcel parcel = Parcel.obtain();
			parcel.unmarshall(bytes, 0, bytes.length);
			parcel.setDataPosition(0);  //extremely important!
			return parcel;
    	}
    	
    }
	
}

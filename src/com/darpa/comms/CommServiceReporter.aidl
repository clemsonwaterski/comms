package com.darpa.comms;

import com.darpa.comms.InMessage;

interface CommServiceReporter {
	void reportInMessage(in InMessage inMessage);
}
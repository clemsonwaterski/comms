package com.darpa.comms;

import android.app.Activity;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

public class CommServiceManager {
	private boolean disconnected = false;
    private CommService commService;
    private static Intent intent = new Intent("com.darpa.comms.MainService");
    private OnConnectedListener onConnectedListener;
    private Service service;
    private Activity activity;

    public static interface OnConnectedListener {
    	void onConnected();
    	void onDisconnected();
    }

	public CommServiceManager(Service service, OnConnectedListener onConnectedListener) {
    	this.service = service;
		this.onConnectedListener = onConnectedListener;
		service.bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
	}
	public CommServiceManager(Activity activity, OnConnectedListener onConnectedListener) {
    	this.activity = activity;
		this.onConnectedListener = onConnectedListener;
		activity.bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
	}
	
	
	public synchronized void disconnect() {
		disconnected = true;
		if(service!=null)
			service.unbindService(serviceConnection);
		else
			activity.unbindService(serviceConnection);
	}
	public synchronized void add(CommServiceReporter reporter) {
		if (disconnected)
			throw new IllegalStateException("Manager has been explicitly disconnected; you cannot call methods on it");
		try {
			commService.add(reporter);
		} catch (RemoteException e) {
			Log.e("CSM", "add reporter", e);
		}
	}
	public synchronized void remove(CommServiceReporter reporter) {
		if (disconnected)
			throw new IllegalStateException("Manager has been explicitly disconnected; you cannot call methods on it");
		try {
			commService.remove(reporter);
		} catch (RemoteException e) {
			Log.e("CSM", "remove reporter", e);
		}
	}

    private ServiceConnection serviceConnection = new ServiceConnection() {
		@Override public void onServiceDisconnected(ComponentName name) {
			Log.d("MSM", "onServiceDisconnected");
			if (onConnectedListener != null)
				onConnectedListener.onDisconnected();
			commService = null;
		}
		@Override public void onServiceConnected(ComponentName name, IBinder service) {
			commService = CommService.Stub.asInterface(service);
			if (onConnectedListener != null)
				onConnectedListener.onConnected();
		}
	};
}
